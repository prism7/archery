# 'Amelia' is an Arch Linux installer, written in Bash. 

(Shellchecked)

-----------------------------------------------------
The main concept behind this installer is to take full advantage of the extended automation that contemporary linux technologies such as systemd has to offer.

'Amelia' is mainly targeted towards the experienced user that needs a quick installation means.

Yet, it is guided, interactive and intuitive, so that probably everyone can use it.



- ## Automation:

There is no support for non-GPT platforms, as this installer makes absolute use of uEFI's 'Discoverable Partitions Specification'.

Consequently, it will automate the detection of the underlying/involved partitions, perform sanity checks based on your preferences, and auto-mount/auto-activate these partitions when necessary (e.g. swap), without the use of the fstab file.

By using systemd's latest and greatest features, configuration needed for eg. swapping, hibernation etc, is a thing of the past.

The system manager will automatically and optimally configure parts of the system, which makes the installation process more robust and of course much more simplified and reliable.

In particular, when the 'ext4' filesystem is being used, the 'genfstab' command is not even executed in the script, and your 'fstab' file will be empty (except only if 'Swapfile' use is desired), as systemd's automation takes care of it.

In the same manner, 'systemd' (instead of 'base' & 'udev') will be used in your initramfs, as it provides the tools for the said automation.

The installation process completes in "one-go", meaning after it's over and the system has rebooted, you're done.


- ## Interaction:

There is full interaction with the user.


### Visual interaction:

The installer follows a menu-driven, step-by-step principal, presenting you with a sane sequence of installation steps, aided by colored prompts, making the installation process easy, pleasant and intuitive.

For disk partitioning
 there's 3 available Modes.
 
An 'Auto Mode' that offers ready-made compatible Partition Layout Presets with sane defaults to select from (Partition Manager stage).

A 'Manual Mode', where "cgdisk" is used, as it offers a TUI (ncurses) interface, making the process of managing the partition table, setting GUIDs etc. easy and safe(r) (Partition Manager stage).

A 'Smart Mode' that engages if the user has selected the 'Auto-Guided Installation' and will select a partitioning profile/preset based on current user preferences, (no additional user interaction needed). If 'Swap' has been selected, the user is offered to specify desired size.


### Input interaction:

You will be asked to make your own choices, and there will always be a confirmation upon success or failure of the outcome of those choices.

You will definitely know what your choice was, what has succeded and if anything went wrong.

The installer has been purposely made in such a way that it will exit if unresolvable errors occur, so it ^should be^ impossible to end up with an errored installation or with parts unfinished.

In several cases, it will try to remedy the situation by taking certain steps (e.g. unmounting a partition, in case it was already mounted from before) and bring you a few steps back, to re-run installation stages that might help you continue with success.

All stages are informative.


Where applicable, system file configuration takes place EXCLUSIVELY in the corresponding drop-in directories and never at the original '.conf' files, so the installed system preserves its functionality across updates and maintenance is minimized.



# Installer overview:


- ### First Check - Warning: [Auto]

A check/detection takes place, notifying you about the mode the installer runs on, along with the potential hazzard of running as root.

Normally should be runing as root, off the Arch-installation media.
If that's not the case, you will be informed accordingly.

If there is 'terminus-font' detected (already included in the Arch installation media), you will be asked to use this font or its HiDPI version, while at console (tty), or you will be prompted to change tty and use console (if you're just simply test-running the installer in your own system with X or Wayland).

If not, setting different fonts is skipped.


- ### Uefi Mode Verification: [Auto]

Checks if the platform runs in uefi mode, and exits if not.


- ### Internet Connection Check: [Auto]

Pings the Arch servers for connection and exits if none is found.


- ### System Clock Update: [Auto]

Self-explanatory.


- ### Machine Detection: [Interactive - Skippable]

The installer informs about the pc-system vendor, model, platform {desktop/laptop/server/vm} and cpu microcode to be installed, ensuring the necessary '*-ucode' package will be installed later on.

Then the installer asks to disable Watchdogs in the installed system (if not on a critical platform, like a server) 


## Main Menu:

- [ ] [1] Personalization
- [ ] [2] System Configuration
- [ ] [3] Disk Management
- [ ] [4] Start Installation


### Personalization Submenu:

- [ ] [1] Locale & Keyboard Layout Selection
- [ ] [2] User, Root User & Hostname Setup
- [ ] [ ] Return to Main Menu


### System Configuration Submenu:

- [ ] [1] Kernel, Secureboot Signing, Bootloader & ESP Mountpoint
- [ ] [2] Filesystem & Swap Selection
- [ ] [3] Graphics Setup
- [ ] [4] Desktop Setup
- [ ] [5] EFI Boot Entries Deletion
- [ ] [6] Wireless Regulatory Domain Setup
- [ ] [ ] Return to Main Menu


### Disk Management Submenu:

- [ ] [1] Disk GPT Manager
- [ ] [2] Partition Manager
- [ ] [ ] Return to Main Menu

-----------------------------------------------

- ### Locale Selection: [Interactive - Skippable]

Set your locale, choose from the locale list or use default.

Skip for Default: en_US.UTF-8


- ### Keyboard Layout Selection: [Interactive - Skippable]

Set your keyboard layout, choose from the keyboard layout list or use default.

Skip for Default: us


- ### User Setup: [Interactive]

Set username, password and verify.


- ### Root User Setup: [Interactive]

Set password and verify.


- ### Hostname Setup: [Interactive]

Self-explanatory.


- ### Kernel Selection: [Interactive]

Select between Linux, Linux LTS, Linux Hardened & Linux Zen Kernels.

- ### Secureboot Signing: [Interactive]

Select to sign (or not) all needed binaries for enabling Secure Boot

If Secure Boot is selected, the installer will offer to create a 'Rescue' bootloader entry, especially necessary for UKIs, as access to edit the menu entry is not allowed.

- ### Bootloader Selection: [Interactive]

Select between Systemd-boot or Grub


- ### ESP Mountpoint Selection: [Interactive]

Select between /efi or /boot as your mountpoint for ESP


- ### Filesystem Selection: [Interactive]

Select Filesystem to be used (Ext4 / Btrfs)

If 'Ext4' is selected: 

You will be asked if you'd like a seperate '/home' partition to be created.

Also, a filesystem performance boost will take place by setting the 'fast_commit' option by default.

If 'Btrfs' is selected:

You will be asked to label your 'snapshots' directory.

If 'Btrfs' & 'Grub' bootloader are selected, the 'grub-btrfs' package will be installed, and the 'grub-btrfsd' service will be automatically enabled, for user convenience. 


- ### Swap Selection: [Interactive - Skippable]

Select between Swap partition, Swapfile, Zram Swap or none.

If 'Swapfile' is selected, then set desired 'Swapfile' size (Swapfile creation & activation and configuration of the 'fstab' file takes place automatically.

Skip if "none" is selected.

Next, the installer asks the user to enable 'systemd-oomd', enhancing 'OOM' management, which will be optimally done by the system manager.


- ### Graphics Setup: [Interactive - Skippable]

A check/detection takes place, notifying you about the underlying graphics subsystem.


If Dual/Triple graphics setup is detected, you will be asked to select for which graphics hardware to automatically have drivers installed, hardware acceleration enabled and the graphics subsystem configured (mkinitcpio.conf, kernel parameters etc).


If Virtual-Machine graphics is detected, the setup is skipped, and graphics configuration defaults to 'no'.


If single graphics are detected, you will be offered the choice to automatically have drivers installed, hardware acceleration enabled and the graphics subsystem configured (mkinitcpio.conf, kernel parameters etc).

Notice that only the latest available drivers will be installed, so should you select 'yes' to auto-configure, ensure your graphics subsystem is currently supported.

Xorg DXX drivers (xf86-xxxx-xxxx) for Intel - AMD - Nvidia will NOT be automatically installed.

In case of AMD graphics, 'amdgpu' driver support for 'Southern Islands' and 'Sea Islands' graphics is offered through auto-configuring.

In case of Nvidia graphics, according to the gpu architecture, there's support for the newer 'nvidia-open' drivers.

Also, support for disabling Nvidia's 'GSP' is offered, when using the 'nvidia' proprietary driver.

If needed, the 'Nvidia Hook' will be automatically created.

The purpose of this part of the installer is not to replace specialized or complicated/sofisticated software but only to offer support for a quick start.


- ### Desktop Setup [Interactive]

In this step, you will be presented with a list of setups to choose from:

- [ ]  1. Plasma
- [ ]  2. Minimal Plasma + Desktop Apps + System Optimizations
- [ ]  3. Gnome
- [ ]  4. Minimal Gnome + Desktop Apps + System Optimizations
- [ ]  5. Xfce
- [ ]  6. Cinnamon
- [ ]  7. Deepin
- [ ]  8. Budgie
- [ ]  9. Lxqt
- [ ] 10. Mate
- [ ] 11. Basic Arch Linux (No GUI)
- [ ] 12. Custom Arch Linux
- [ ] 13. Cosmic (ALPHA)
---------------------------------------------------------------

- All desktop setups, except 'Minimal Plasma' & 'Minimal Gnome', are completely 'Vanilla', and only come with network support (networkmanager).

For any additional functionality, please consult the Archwiki.

- The installer offers the convenient option to set your own kernel parameters for boot, on-the-fly, while at 'Desktop Selection' stage.

- 'Minimal Plasma' setup will install a minimal, system-optimized KDE Plasma desktop plus additional every-day use software.

- 'Minimal Gnome' setup will install a minimal, system-optimized Gnome desktop plus additional every-day use software.

- 'Basic Arch Linux' is literally a basic Arch linux system, consisting of just the following packages: "base, linux{lts-hardened-zen}, linux-firmware (only if on bare-metal), sudo, *-ucode, nano, pkgstats, vim, networkmanager, wireless-regdb and e2fsprogs/btrfs-progs" (depending on the filesystem selected).

- 'Custom Arch Linux' consists of the following packages: "base, linux-firmware (only if on bare-metal), sudo, pkgstats, and your current installation preferences/choices.
In this step you can create your own system (using a Basic Arch Linux system as base), on-the-fly, using the following Menus:


- ### Custom Arch Linux [Interactive]

- [ ]  Add Your Packages    [Mandatory]
- [ ]  Add Your Services    [Skippable]
- [ ]  Add Your Kernel Parameters   [Skippable]
---------------------------------------------------------------

- ### EFI Boot Entries Deletion: [Interactive - Skippable]

Choose if you wish to delete any EFI boot entries or skip.

If a 'VM' is detected, this stage is skipped.


- ### Wireless Regulatory Domain Setup: [Interactive - Skippable]

Enter your 2-letter Country Code (e.g. 'US') to set the correct wifi regulations for your country.

If a 'VM' is detected, this stage is skipped.


- ### Disk GPT Manager: [Interactive - Skippable]

Use 'gdisk' to perform various disk operations to any detected drive in your system (e.g. zapping the GPT and create a new one, etc.


- ### Partition Manager: [Interactive - Skippable]

The'Partition Manager' stage consists of 2 Modes:


An 'Automatic Mode' consisting of 2 sub-modes:

'Smart Mode': [Interactive - Skippable]
 Detects user's preferences and partitions the selected disk accordingly (no user interaction needed).

'Partitioning Presets': [Interactive - Skippable]
Offers ready-made 'Partition Layout Presets' with sane defaults to select from.

A 'Manual Mode': [Interactive - Skippable]
It shows extended info about the supported partition types and mountpoints that the installer expects.

The user will manage the involved disk(S) manually, using 'cgdisk', with its easy and intuitive ncurses TUI.


Existence of an "EFI" System Partition and a "Root x86-64" Partition is mandatory and expected.

The partitions layout in this stage should reflect your previous choices, e.g. if you chose to use a 'Swap' partition, and it does not exist, now is the time to create it.

If you need a seperate 'Home' partition, create it now or an existing one can also be used.

For the systemd 'auto-gpt-generator' automation to work, all involved partitions MUST reside in the same physical disk/device.


- ### Installation Disk Selection & Sanity Check: [Auto]

Select a disk to install Arch Linux on.

This step incorporates and performs a Sanity Check on the chosen disk.

This means the installer will verify that the choices you've made so far, are correctly reflected on the partitions layout, before it lets you continue with the installation.

The aim here is to make the installation process error-proof.

It will ensure that an 'EFI' System Partition indeed exists, that a 'Root x86-64' partition exists, that a 'Swap' partition exists IF you chose to use a 'Swap' partition and will also detect an existing seperate 'Home' partition.

If 'systemd-boot' is selected and '/efi' mountpoint for an ESP smaller than 200Mb is selected, the installer expects an 'XBOOTLDR' extended boot partition to be present/user created.

On top of all that, the installer will scan the partitions on the installation disk and if more than one of each type {root/EFI/home/swap/xbootldr} are detected, then it will Auto-Assign the 1st partition of each (involved) type, to be used by systemd's automation in the installation (as the 'Discoverable Partitions Specifications' dictate).
It comes with its own menu/prompts, for proper user interaction.

For extended clarity reasons, A TUI partition/filesystem presentation is presented 
when multiple partitions of the same type {root/EFI/home/swap/xbootldr} have been detected and the user needs to CONFIRM the Automated Selection made by the installer.

If the Sanity Check fails, the installer will smartly load any necessary stages, so you can correct any errors/omissions. 


- ### Encryption Setup: [Interactive - Skippable]

Choose if you will enable LUKS encryption on your 'Swap/Root/Home' partitions

Also, you will be asked to Label your partitions.

Again, the scope here is not to create an inpenetrable system but a decently protected system.

Using 'Secure Boot' with an unencrypted 'EFI System Partition' and a LUKS encrypted 'Root/Home/Swap' partition(s) should suffice.


- ### Swap Partition Activation: [Auto]

If you chose to create a 'Swap' partition, now it will be activated.


    ### If the answer to the "Disk Encryption" step was "no" :


- ### Mode Selection: [Interactive]

The installer asks the user to select the preferred disk preparation mode:

- [ ] 'Auto' will perform Formatting, Labeling and Mounting of all of the involved partitions automatically, but it will ask for your confirmation before formatting a separate ext4 'Home' partition (if found), in case you would like to keep it intact.

- [ ] 'Manual' mode will perform the above by asking the user to select which partitions to format, choose desired name to label the partitions, and mount them accordingly.

The 'Auto' mode will switch to 'Manual' mode if errors are encountered (so the user gets control of the procedure), and when the said mini-step is completed (e.g formatting 'EFI') it will continue in 'Auto' mode for the rest of the partitions.

Upon completion, both 'Auto' and 'Manual' mode present the user with a summary of the partitions and mounts layout, preparing for the next step.


- ### Confirm Installation Status [Interactive]

Decide if you will procceed with the installation.

If you are satisfied with the result (presented to you in the previous step and still showing on screen), answer 'yes'.

If your answer is 'no', then the installer unmounts all mounted partitions and reloads 'System Configuration' and all necessary installation stages, so you can revise your choices until you are satisfied with the outcome.


- ### Optimize Pacman: [Interactive]

Sometimes, the mirrorlist created from Reflector's auto-run at start-up is not ideal.

So, in this step you can select your preferred country, from the current list of countries that are hosting Archlinux mirrors.

If the field is left empty, the default Reflector's mirrorlist will be used.

If a country from the list is chosen, Reflector will rate its mirrors, using [ l 10 -p https -f 10 ].
If no mirrors are found, it means that the country chosen doesn't fulfill Reflector's set criteria.
Try with another country.

Next, you will be asked to enable Pacman's 'Multilib' repository in the installed system, if desired.

- ### Pacstrap System: [Auto]

The system will be pacstrapped and your selected setup will be installed.


- ### Chroot & Configure System: [Auto]

During this stage, the majority of configuration takes place.

Detection of rotational or solid state drives has already taken place, so 'fstrim.timer' will be activated accordingly for the installation drive.

Also, if a non-rotational drive is being used during installation, and LUKS encryption has been chosen, then specific options will be applied to open the LUKS container.

Graphics setup, encryption setup, swap/swapfile/Zram Swap activation, offset calculation, specific filesystem-based options, pciid database update, auto configuration of your timezone based on your computer's ip address, makepkg optimization, sysctl / mkinitcpio / udisks / systemd / sudoers file configuration, systemd services activation etc. all happen here.

As mentioned earlier, configuration takes place using '.conf' files ONLY in the respective drop-in directories and never editing the original '.conf' files, where applicable.

'Minimal Plasma' & 'Minimal Gnome' setups offer a few system optimizations.

All other Vanilla setups use the basic/required configuration to make your system run.


    ### If the answer to the "Disk Encryption" step was "yes":


- ### Secure Disk Erasure: [Interactive - Skippable]

Since LUKS encryption has been selected in a previous step, the installer offers the option to securely erase the involved drive before continuing with the installation, if so desired.


- ### LUKS Encryption: [Interactive - Skippable]

Your selected 'Root x86-64' partition will be encrypted using LUKS.

If a separate 'Home' partition is detected, and the filesystem chosen is 'ext4', then you will be offered the option to encrypt 'Home' too.

Should you choose 'yes' to that option, then auto-configuration will take place, so an extra LUKS password won't be needed to unlock said volume.

Should you choose 'no', the installer will still ask for your confirmation before formatting the 'Home' partition to 'ext4', in case you would like to keep it intact.

This stage cannot offer automatic error-resolving by automatically taking actions such as re-running previous steps of the installation precedure, like [Auto] or [Manual] mode does, as after LUKS encryption has finished, partprobing and informing the kernel about further/new changes requires a system restart.

As a result, if any errors occur at this stage, will eventually terminate the installation, and you will be required to reboot and re-run the installer.

For the same reason, the installer cannot offer the choice of reloading previous phases, as it does in the 'Confirm Installation Status' stage.

From here onwards, all stages are the same as the Non-Encrypted Installation.

-----------------------------------------------------------------------------


That's pretty much it.

All instructions used, stem from the Arch wiki.

Any feedback will be greatly appreciated.

Have fun everyone!

